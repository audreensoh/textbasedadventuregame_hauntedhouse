#ifndef INVENTORY_H
#define INVENTORY_H

//**Inventory which contains object belongs to player

#include<string>
#include<vector>
#include "Objects.h"

class Inventory
{
private:
	std::vector<Object> object;

public:
	Inventory();
	Inventory(std::vector<Object>& objects);
	~Inventory();

	void addToInventory(Object& o);
	void removeFromInventory(Object o);
	void removeFromInventory(std::string& n);
	bool checkInInventory(std::string n);

	Object* getObjectByName(std::string& n);

	std::vector<Object> getObjects();
	void printInventory();

	void saveInventory();
};

#endif