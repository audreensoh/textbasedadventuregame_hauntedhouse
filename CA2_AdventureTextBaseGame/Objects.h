#ifndef object_H
#define object_H

//**Contains objects details

#include<fstream>
#include<string>

class Object
{
private:
	//**all variables including object name, ojbect description etc.
	std::string oname;
	std::string odescription;
	std::string owhere;
	std::string ouseWith;
	std::string omove;
	bool opickup;
	bool omovable;
	bool ouseable;
	bool oinvisible;
	bool oreadable;
	bool odrinkable;


public:
	Object();
	Object(std::string name, std::string description, std::string where, std::string useWith, bool pickup, bool movable, std::string move, bool drink, bool useable, bool invisible, bool readable);
	~Object();
	std::string examin();

	std::string getObjectName();
	void setObjectName(std::string name);
	std::string getObjectDescription();
	void setObjectDescription(std::string Desc);
	std::string getObjectWhere();
	void setObjectWhere(std::string where);
	void setObjectInvisible(bool visible);
	bool getObjectInvisible();
	void setObjectpickup(bool pickup);
	bool getObjectpickup();
	void setObjectMoveable(bool move);
	bool getObjectMoveable();
	void setObjectDrinkable(bool drink);
	bool getObjectDrinkable();
	std::string getmove();
	void setusewith(std::string usewith);
	std::string getUseWith();
	void setObjectUsable(bool use);
	bool getObjectUsable();
	void setObjectReadable(bool read);
	bool getObjectReadable();
	void setmovedDesc(std::string moved);


	//**to print object, but not used often
	void printObject();




};

#endif