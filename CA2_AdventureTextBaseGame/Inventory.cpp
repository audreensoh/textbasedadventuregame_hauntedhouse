#include"Inventory.h"
#include<iostream>
#include<fstream>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::ofstream;
//using namespace std;

//**default constructor
Inventory::Inventory()
{

}

Inventory::Inventory(vector<Object>& objects)
{
	object = objects;
}

Inventory::~Inventory()
{

}

void Inventory::addToInventory(Object& o)
{
	object.push_back(o);
}

void Inventory::removeFromInventory(Object o)
{
	bool foundItem = false;
	for (vector<Object>::size_type i = 0; i != object.size(); i++)
	{
		if (object[i].getObjectName() == o.getObjectName())
		{
			object.erase(object.begin() + i);
			foundItem = true;
			break;
		}
	}
	if (foundItem == false)
	{
		cout << "Object not found in inventory." << endl;
	}
}

void Inventory::removeFromInventory(string& n)
{
	bool foundItem = false;
	for (vector<Object>::size_type i = 0; i != object.size(); i++)
	{
		if (object[i].getObjectName() == n)
		{
			object.erase(object.begin() + i);
			foundItem = true;
			break;
		}
	}
	if (foundItem == false)
	{
		cout << "Object not found in inventory." << endl;
	}
}

bool Inventory::checkInInventory(string n)
{
	bool foundItem = false;
	for (vector<Object>::size_type i = 0; i != object.size(); i++)
	{
		if (object[i].getObjectName() == n)
		{
			//cout << "Found item" << endl;
			foundItem = true;
			break;
		}
	}
	if (foundItem == false)
	{
		//cout << "Checked but no object found" << endl;
	}

	return foundItem;
}

Object* Inventory::getObjectByName(string& n)
{
	bool foundItem = false;
	for (vector<Object>::size_type i = 0; i != object.size(); i++)
	{
		if (object[i].getObjectName() == n)
		{
			foundItem = true;
			return &object[i];
			break;
		}
	}
	if (foundItem == false)
	{
		cout << "no object found" << endl;
	}
	return NULL;
}

vector<Object> Inventory::getObjects()
{
	return object;
}

void Inventory::printInventory()
{
	cout << "Inventory:" << endl;
	for (vector<Object>::size_type i = 0; i != object.size(); i++)
	{
		cout << "Item " << i + 1 << " : " << object[i].getObjectName() << endl;
	}
}

void Inventory::saveInventory()
{
	ofstream myfile;
	myfile.open("SaveInventory.txt");
	myfile << "NumberObjectInInventory= " << object.size() << endl;
	for (int i = 0; i != object.size();i++)
	{
		myfile << "Object= " << object[i].getObjectName() << endl;
		myfile << "description= " << object[i].getObjectDescription() << endl;
		myfile << "Where= " << object[i].getObjectWhere() << endl;
		myfile << "pickup= " << object[i].getObjectpickup() << endl;
		myfile << "movable= " << object[i].getObjectMoveable() << endl;
		myfile << "move= " << object[i].getmove() << endl;
		myfile << "useable= " << object[i].getObjectUsable() << endl;
		myfile << "invisible= " << object[i].getObjectInvisible() << endl;
		myfile << "useWith= " << object[i].getUseWith() << endl;
		myfile << "readable= " << object[i].getObjectReadable() << endl;
		myfile << "drinkable= " << object[i].getObjectDrinkable() << endl;
	}
}