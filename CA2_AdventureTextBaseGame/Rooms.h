#ifndef Room_H
#define Room_H

//**includes all room's details loaded from the text file

#include<fstream>
#include<string>
#include<vector>
#include"Objects.h"

class Room
{
private:
	int roomNumber;
	std::string roomName;
	std::string roomDesc;
	int roomExit;
	//put the exits of each of the rooms in vector
	std::vector<std::string> allExits;
	//put the objects of each rooms in allobject vector
	std::vector<Object> allobject;
	bool roomLocked;
	Object o;

public:
	Room();
	Room(int number, std::string name, std::string desc,bool locked, int exitno, const std::vector<std::string>& exitPlace);
	void setRoomNumber(int n);
	int getRoomNumber();
	void setRoomName(std::string n);
	std::string getRoomName();
	void setRoomDesc(std::string d);
	std::string getRoomDesc();
	void setlocked(bool n);
	bool getlocked();
	void setExitNo(int n);
	int getexitno();
	void setExits(std::vector<std::string> exit);
	std::vector<std::string>getExits();
	void printRoom();
	void saveObjects(std::ofstream& myfile);

	//**to check if player can exit to the room they wanted to go
	bool findExit(std::string e);

	void addObject(Object& o);
	bool removeObject(std::string& o);
	//**to check in the allobject vector if the object is in the current room if an object name is entered.
	bool checkObjectInRoom(std::string& n);
	//**Object pointer to return that object details if object name entered is matched with any object names in vector
	Object* Room::getObject(std::string& n);
	void printObject();

	std::vector<Object> getAllObject();


};
#endif