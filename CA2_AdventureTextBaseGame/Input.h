#ifndef INPUT_H
#define INPUT_H

//**get input and split the line into words and put each word into vector

#include<string>
#include<vector>

std::vector<std::string> getInput();
std::vector<std::string> split(const std::string& s);

#endif