#include<iostream>
#include<string>
#include<fstream>
#include <windows.h>
#include "Map.h"
#include "GameFunctions.h"
#include "ParseInput.h"
#include "Player.h"
#include "StringFunction.h"
#include "GhostStore.h"

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::getline;
//using namespace std;

void Intro()
{
	//to set the command prompt size.
	system("mode 80,80");
	center("Haunted house");
	center("");

	//logo goes here
	center(" ");
	center("                        �         �                          ");
	center("                         �         �                         ");
	center("                     �   �         �   �                     ");
	center("                     �  ��         ��  �                     ");
	center("                     �� ���       ��� ��                     ");
	center("             �      ��   ���     ���   ��      �             ");
	center("            ��      ��   ���     ���   ��      ��            ");
	center("           ��      ��    ����   ����    ��      ��           ");
	center("           ��     ���    ����  �����    ���     ���          ");
	center("       �  ���    ����    ����   ����    ����   ����  �       ");
	center("       �� �����  ����   �����   �����   ����  ����� ��       ");
	center("       �� �����  �����������     �����������  ����� �        ");
	center("       �� �����  �����������     �����������  ����� ��       ");
	center("      ���  ����   �������������������������   ����  ���      ");
	center("     ����  ����   �������������������������   ����  ����     ");
	center("    ����   ����� ��������������������������� �����   ����    ");
	center("   ����    ����������������������������������������   ����    ");
	center("   �����  �����������������������������������������  ����    ");
	center("    ������������������������������������������������������    ");
	center("    �����������������������������������������������������    ");
	center("     ���������������������������������������������������     ");
	center("      �������������������������������������������������      ");
	center("     �����           �������������������           �����     ");
	center("     ������             �������������             ������     ");
	center("      �������        ..     ���������     ..      �������  ");
	center("       ��������             �����             ��������       ");
	center("        ����������           ���           ����������        ");
	center("           ���������������������������������������           ");
	center("              ���������������   ���������������              ");
	center("                  ����������     ����������                  ");
	center("                   ��������       ��������                   ");
	center("                  ���������       ���������                  ");
	center("                  ��������� ����� ���������                  ");
	center("                 ���������������������������                 ");
	center("                 ���  �����������������  ���                 ");
	center("                  ��  ����  �����  ����  ��                  ");
	center("                      ����  �����  ����                     ");
	center("");
	center("");
	center("");

	center("Discover the insides of the haunted house.");
	center("");


	mainMenu();
}

bool resume = false;

void mainMenu()
{
	int x = 0;


	if (getSaved() == true)
	{
		center("Select: ");
		center("1. Tutorial");
		center("2. Restart Game");
		center("3. Exit");
		center("4. Resume");

		do
		{
			cin >> x;
		} while (x != 1 && x != 2 && x != 3 && x != 4);
	}
	else{
		center("Select: ");
		center("1. Tutorial");
		center("2. Play Game");
		center("3. Exit");
		int x = 0;
		do
		{
			cin >> x;
		} while (x != 1 && x != 2 && x != 3);
	}

	

	mainMenuChoice(x);
}

void mainMenuChoice(int x)
{
	system("CLS");
	if (x == 1)
	{
		cout << "Commands: " << endl;
		cout << "   - Go to (place name)/ Go (place name)" << endl;
		cout << "   - pick up (something)" << endl;
		cout << "   - move (something)" << endl;
		cout << "   - read (something)" << endl;
		cout << "   - inventory" << endl;
		cout << "   - examine" << endl;
		cout << "   - use (sth)" << endl;
		cout << "   - health" << endl;
		cout << "   - Exit" << endl;


		system("Pause");
		system("CLS");
		Intro();

	}
	else if (x == 2)
	{
		startGame();
	}

	else if (x == 3)
	{
		center("Goodbye");
		system("PAUSE");
		exit(0);
	}
	else if (x == 4)
	{
		resume = true;
		startGame();
	}
}


void loadMap(Map& Map1)
{

	ifstream inFile("rooms.txt");

	//**If file can be opened then load it into Maps, if not show error message.
	if (inFile)
	{
		//cout << "Loading Rooms" << endl;
		Map1.loadRooms(inFile);
	}
	else
	{
		cout << "Error: Failed to load Rooms." << endl;
	}
	//**Loading the objects into the rooms.
	ifstream inFile2("Objects.txt");

	//**If file can be opened then load it into Maps, if not show error message.
	if (inFile2)
	{
		//cout << "LoadingObjects" << endl;
		Map1.loadObjects(inFile2);
		//cout << "After Loading Objects" << endl;
	}
	else
	{
		cout << "Error: Failed to load Objects." << endl;
	}

	


}

void loadGhost(GhostStore& gs)
{
	ifstream inFile3("Ghost.txt");

	if (inFile3)
	{
		gs.loadGhost(inFile3);
	}
	else
	{
		cout << "Error: Failed to Ghost." << endl;
	}
}

void loadPlayer(Player& p)
{
	string Name;
	cout << "Please enter name(one word):" << endl;
	cin >> Name;





	p.setName(Name);
	p.setHealth(100);
	
	//**have to ignore first enter when the user enters their name or it will be showing twice input and first input will show error.
	cin.ignore(1);
}

void loadSavedMap(Map& Map1)
{
	ifstream inFile("SaveRoom.txt");

	//**If file can be opened then load it into Maps, if not show error message.
	if (inFile)
	{
		//cout << "Loading Rooms" << endl;
		Map1.loadRooms(inFile);
	}
	else
	{
		cout << "Error: Failed to load Rooms." << endl;
	}
	//**Loading the objects into the rooms.
	ifstream inFile2("SaveObject.txt");

	//**If file can be opened then load it into Maps, if not show error message.
	if (inFile2)
	{
		//cout << "LoadingObjects" << endl;
		Map1.loadObjects(inFile2);
		//cout << "After Loading Objects" << endl;
	}
	else
	{
		cout << "Error: Failed to load Objects." << endl;
	}

}

void loadSavedPlayer(Player& p)
{
	ifstream myfile("SavePlayer.txt");

	if (myfile)
	{
	string name;
	int health;
	int experience;
	string garbage;
	myfile >> garbage >> name;
	myfile >> garbage >> health;
	myfile >> garbage >> experience;

	p.setName(name);
	p.setHealth(health);
	p.setExperience(experience);

	}	
	
	int totalNumOfObject = 0;
	string object;
	string desc;
	string where;
	bool pickup;
	bool moveable;
	string move;
	bool useable;
	bool invisible;
	string usewith;
	bool readable;
	bool drinkable;
	string garbage;

	ifstream inFile("SaveInventory.txt");

	if (inFile)
	{
	inFile >> garbage >> totalNumOfObject;
	//cout << totalNumOfObject << endl;
	for (int i = 0; i != totalNumOfObject; i++)
	{

		inFile >> garbage >> object;
		//cout << object << endl;
		inFile >> garbage;
		getline(inFile, desc);
		//cout << desc << endl;
		inFile >> garbage >> where;
		//cout << where << endl;
		inFile >> garbage >> pickup;
		//cout << pickup << endl;
		inFile >> garbage >> moveable;
		inFile >> garbage;
		getline(inFile, move);
		//cout << move << endl;
		inFile >> garbage >> useable;
		inFile >> garbage >> invisible;
		inFile >> garbage >> usewith;
		inFile >> garbage >> readable;
		inFile >> garbage >> drinkable;

		Object loadObject(object, desc, where, usewith, pickup, moveable, move, drinkable, useable, invisible, readable);


			p.pickup(loadObject);
		}
	}
}

void loadSavedCurrentRoom(Map& m)
{
	ifstream myfile("SaveCurrentRoom.txt");
	string name;
	string garbage;
	if (myfile)
	{
		myfile >> garbage >> name;
		m.setCurrentRoom(name);
	}


}

void startGame()
{
	//**create map
	Map myMap;
	//create player
	Player player;
	GhostStore gstore;

	if (resume == true)
	{
		loadSavedPlayer(player);
		loadSavedMap(myMap);
		loadGhost(gstore);
		loadSavedCurrentRoom(myMap);
	}
	else
	{
		//**ask player for name
		loadPlayer(player);
		//**read map and object from textfile
		loadMap(myMap);
		//myMap.printAllRoom();
		loadGhost(gstore);
		//**set starting room as livingroom so when game starts it shows you are in living room.
		myMap.setCurrentRoom("livingroom");
	}

	//cout << "Printing current room" << endl;
	//**print out current room in the beginning of game
	myMap.printCurrentRoom();
	//myMap.getCurrentRoom().printObject();

	do{
		//**get input from user till user finish the mission or when they die or gave up half way
		parse(myMap, player,gstore);

	} while (player.getHealth() > 0 && player.getHealth() <101 );

	
	if (player.getHealth() == 0)
	{
		cout << endl;
		cout << endl;
		cout << "Mission Fail!" << endl;
		cout << "You lost!" << endl;
	}
	else if (player.getHealth()>100)
	{
		cout << endl;
		cout << endl;
		cout << "Mission Accomplished!" << endl;
		cout << "Well Done!!You got out of the haunted house!!" << endl;
		int playerEx = player.getExperience() + 1;
		player.setExperience(playerEx);
		cout << "Your Experience: " <<playerEx << endl;
	}



}

