#include<iostream>
#include<string>
#include<vector>
#include<fstream>
#include"Map.h"
#include"Rooms.h"
#include"Objects.h"

//using namespace std;
using std::cout;
using std::endl;
using std::string;
using std::getline;
using std::vector;
using std::ifstream;
using std::ofstream;

Map::Map()
{
	//**reference: from Lukas's game code
	//**initialize pointer with empty room
	currentRoom = new Room;
}
Map::Map(vector<Room> rooms)
{
	allRooms = rooms;
}
Map::~Map()
{
	//deleting the instance of new room
	//delete currentRoom;
}
void Map::loadRooms(ifstream& inFile)
{
	int totalRooms = 0;
	int roomNumber = 0;
	string roomName = "";
	string roomDesc = "";
	int roomExit = 0;
	string exit = "";
	string garbage = "";
	bool locked = 0;
	inFile >> garbage >> totalRooms;
	//cout << totalRooms << endl;

	for (int i = 0; i != totalRooms; i++)
	{

		Room loadMyRoom;

		inFile >> garbage >> roomNumber;
		//cout << roomNumber << endl;
		inFile >> garbage >> roomName;
		//cout << roomName << endl;
		inFile >> garbage;
		getline(inFile, roomDesc);
		inFile >> garbage >> locked;
		//cout << roomDesc << endl;
		inFile >> garbage >> roomExit;
		//cout << roomExit << endl;

		vector<string> allExits;
		for (int r = 0; r != roomExit; r++)
		{
			inFile >> garbage >> exit;
			//cout << exit << endl;
			allExits.push_back(exit);
		}
		//cout << endl;
		loadMyRoom.setRoomNumber(roomNumber);
		loadMyRoom.setRoomName(roomName);
		loadMyRoom.setRoomDesc(roomDesc);
		loadMyRoom.setlocked(locked);
		loadMyRoom.setExitNo(roomExit);
		loadMyRoom.setExits(allExits);

		allRooms.push_back(loadMyRoom);
		//loadMyRoom.printRoom();

	}


}

void Map::loadObjects(std::ifstream& inFile)
{
	
	//cout << "Loading objects load" << endl;
	int totalNumOfObject = 0;
	string object;
	string desc;
	string where;
	bool pickup;
	bool moveable;
	string move;
	bool useable;
	bool invisible;
	string usewith;
	bool readable;
	bool drinkable;
	string garbage;

	inFile >> garbage >> totalNumOfObject;
	//cout << totalNumOfObject << endl;
	for (int i = 0; i != totalNumOfObject; i++)
	{
		
		inFile >> garbage >> object;
		//cout << object << endl;
		inFile >> garbage;
		getline(inFile, desc);
		//cout << desc << endl;
		inFile >> garbage >> where;
		//cout << where << endl;
		inFile >> garbage >> pickup;
		//cout << pickup << endl;
		inFile >> garbage >> moveable;
		inFile >> garbage;
		getline(inFile, move);
		//cout << move << endl;
		inFile >> garbage >> useable;
		inFile >> garbage >> invisible;
		inFile >> garbage >> usewith;
		inFile >> garbage >> readable;
		inFile >> garbage >> drinkable;

		Object loadObject(object, desc,where, usewith, pickup, moveable, move, drinkable, useable,invisible, readable);


		putObjectInRoom(where, loadObject);

	}
}



void Map::saveRooms()
{
	ofstream myfile;
	myfile.open("SaveRoom.txt");
	
	myfile << "TotalRoomNumber= " << allRooms.size() << endl;
	for (int i = 0; i != allRooms.size(); i++)
	{
		myfile << "RoomNumber= " << i << endl;
		myfile << "RoomName= " << allRooms[i].getRoomName() << endl;
		myfile << "RoomDescription= " << allRooms[i].getRoomDesc() << endl;
		myfile << "locked= " << allRooms[i].getlocked() << endl;
		myfile << "RoomsExit= " << allRooms[i].getexitno() << endl;


		for (int r = 0; r != allRooms[i].getExits().size(); r++)
		{
			myfile << "Exit= " <<allRooms[i].getExits()[r] << endl;

		}
	}
}

void Map::saveCurrentRoom(string name)
{
	ofstream myfile;
	myfile.open("SaveCurrentRoom.txt");

	myfile << "CurrentRoom= " << name << endl;
}

void Map::saveObjects()
{
	ofstream myfile;
	myfile.open("SaveObject.txt");

	vector<Object> obj;
	for (int i = 0; i != allRooms.size(); i++)
	{
		for (int r = 0; r != allRooms[i].getAllObject().size(); r++)
		{
			obj.push_back(allRooms[i].getAllObject()[r]);
		}
	}

	myfile << "TotalObjects= " << obj.size() << endl;
	for (int i = 0; i != allRooms.size(); i++)
	{
		
		setCurrentRoom(allRooms[i]);
		(*currentRoom).saveObjects(myfile);
	}

}

void Map::setCurrentRoom(Room& r)
{
	currentRoom = &r;
}

//**setting current room by using room name and it goes to getroom function to check in the vector to see if any room name matches the string.
void Map::setCurrentRoom(string r)
{
	currentRoom = getRoom(r);
}

void Map::setCurrentRoomDesc(string d)
{
	(*currentRoom).setRoomDesc(d);
}

Room Map::getCurrentRoom()
{
	return (*currentRoom);
}

Room* Map::getRoom(string& r)
{
	for (vector<Room>::size_type i = 0; i < allRooms.size(); i++)
	{
		//cout << "get room     " << allRooms[i].getRoomName() << endl;

		if (allRooms[i].getRoomName() == r)
		{
			return &allRooms[i];

			//cout << "Found room" << allRooms[i].getRoomName() << endl;
		}
	}
	cout << "No room found" << endl;
	return NULL;

}

void Map::printAllRoom()
{
	for (vector<Room>::size_type i = 0; i < allRooms.size(); i++)
	{
		allRooms[i].printRoom();
	}

}

bool Map::currentRoomExit(string e)
{
	return (*currentRoom).findExit(e);
}

bool Map::CheckIfObjectInCurrentRoom(std::string& n)
{
	return (*currentRoom).checkObjectInRoom(n);
}

Object* Map::getObjectFromCurrentRoom(std::string& n)
{
	return (*currentRoom).getObject(n);
}

void Map::removeObjectFromCurrentRoom(std::string& n)
{
	(*currentRoom).removeObject(n);
}

void Map::putObjectInRoom(string& n, Object& o)
{
	bool foundRoom = false;

	for (vector<Room>::size_type i = 0; i != allRooms.size(); i++)
	{
		if (allRooms[i].getRoomName() == n)
		{
			//cout << "Put " << o.getObjectName() << " to " << allRooms[i].getRoomName() << endl;
			allRooms[i].addObject(o);
			foundRoom = true;
			break;
		}
	}
	if (foundRoom == false)
	{
		cout << n  << endl;
		cout << "Room not found to put object in" << endl;
	}
}

void Map::printCurrentRoom()
{
	(*currentRoom).printRoom();
}

