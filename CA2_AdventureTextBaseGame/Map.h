#ifndef MAP_H
#define MAP_H

//**Map.h contains rooms and function to set or get sth from room

#include<fstream>
#include<string>
#include<vector>
#include"Rooms.h"
#include"Objects.h"
#include "Ghost.h"

class Map
{
private:
	std::vector<Room> allRooms;
	Room *currentRoom;

public:
	Map();
	Map(std::vector<Room> rooms);
	~Map();
	void loadRooms(std::ifstream& inFile);
	void loadObjects(std::ifstream& inFile);
	void saveRooms();
	void saveCurrentRoom(std::string name);
	void saveObjects();
	void setCurrentRoom(Room& r);
	void setCurrentRoom(std::string r);
	void setCurrentRoomDesc(std::string d);
	Room getCurrentRoom();
	//** to check in the allRooms vector to see if any room name matches the one the player entered
	Room* getRoom(std::string& r);
	//**to check if the room has the exit the player wanted to go to
	//**Extended from room class
	bool currentRoomExit(std::string e);
	//**to check if the object the player wanted in in room
	//**Extended from room class
	bool CheckIfObjectInCurrentRoom(std::string& n);
	//**to get an object that is in current room
	//**Extended from room class
	Object* getObjectFromCurrentRoom(std::string& n);
	//**Extended from room class as remove object function in room class
	void removeObjectFromCurrentRoom(std::string& n);
	//**Extended from room class as add object fuction in room class
	void putObjectInRoom(std::string& n, Object& o);
	Object* getAllObjectFromRoom();

	void printAllRoom();
	void printCurrentRoom();
};


#endif