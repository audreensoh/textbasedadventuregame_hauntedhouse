#include"ParseInput.h"
#include<vector>
#include<iostream>
#include<string>
#include <algorithm>
#include <ctime>
#include <cctype>
#include "Map.h"
#include "Objects.h"
#include"Input.h"
#include"Rooms.h"
#include"Inventory.h"
#include"Player.h"
#include "GhostStore.h"
#include "GameFunctions.h"

using std::vector;
using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::ifstream;
//using namespace std;

//to find the main command words from the line of player input 
bool found(vector<string> i, string f)
{
	//"typedef" allows you to assign another equivalent name for a type
	//so instead of typing vector<string> i use "sentence"
	typedef vector<string> sentence;
	bool truefalse = false;
	for (sentence::iterator iter = i.begin(); iter != i.end(); ++iter)
	{
		if (*iter == f)
		{
			truefalse = true;
			return truefalse;
		}
	}
	truefalse = false;
	return truefalse;
}

void fight(Player& p, GhostStore& ghost)
{
	do{
		cout << "Choose either to Attack or hide" << endl;
		vector<string> input;

		input = getInput();
		int num1 = rand() % 2 + 1;
		int num2 = rand() % 3 + 1;
		if (found(input, "attack"))
		{
			if (num1 == 1)
			{
				cout << "The Ghost got attacked!" << endl;
				int glife = (ghost.getCurrentGhost().getGlife()) - 50;
				ghost.setCurrentGhostLife(glife);
				if (glife < 0)
				{
					glife = 0;
					ghost.getCurrentGhost().setGlife(0);
				}
				cout << "Ghost life: " << glife << endl;
			}
			else
			{
				cout << "You missed that attack!!" << endl;
				cout << "Ghost life: " << ghost.getCurrentGhost().getGlife() << endl;
			}

			if (num2 == 1)
			{
				cout << "The ghost attacked you sucking your spirit our from your body." << endl;
				int plife = (p.getHealth() - 30);
				p.setHealth(plife);
				if (plife < 0)
				{
					plife = 0;
					p.setHealth(0);
					cout << "You died" << endl;
				}
				cout << "Your life: " << plife << endl;
			}
		}
		else if (found(input, "hide"))
		{
			if (num2 == 1)
			{
				cout << "The ghost found you and gave you a killer look in the eye." << endl;
				int plife = (p.getHealth() - 30);
				p.setHealth(plife);
				if (plife < 0)
				{
					plife = 0;
					p.setHealth(0);
					cout << "You died!!" << endl;
				}
				cout << "Your life: " << plife << endl;
			}
			else
			{
				cout << "Good hide!!" << endl;
			}
		}
	} while (p.getHealth() != 0 && ghost.getCurrentGhost().getGlife() != 0);

}

void checkFight(std::vector<std::string> i, Map& m, Player& p, GhostStore& ghost)
{
	if (ghost.getCurrentGhost().getGName() == "AngryGhost")
	{
		fight(p, ghost);
	}
	else
	{
		cout << "No fights available." << endl;
	}
}

void Play()
{
	const int MAX_GUESS_NUM = 12; // range of random number to guess 0 to (this-1)
	const int MAX_NUM_TRIES = 3;  // max number of guesses/tries player gets

	int num = rand() % 2 + 1;

	cout << num << endl;
	if (num == 1)
	{

		const int MAX_WRONG = 8;  // maximum number of incorrect guesses allowed

		vector<string> words;  // collection of possible words to guess
		words.push_back("GUESS");
		words.push_back("HANGMAN");
		words.push_back("DIFFICULT");

		srand((unsigned int)time(0));
		random_shuffle(words.begin(), words.end());

		const string THE_WORD = words[0];         // word to guess
		int wrong = 0;                            // number of incorrect guesses
		string soFar(THE_WORD.size(), '-');       // word guessed so far
		string used = "";                         // letters already guessed

		cout << "The ghost favourite game is hangman. Guess the word!!";

		// main loop
		while ((wrong < MAX_WRONG) && (soFar != THE_WORD))
		{
			cout << "\n\nYou have " << (MAX_WRONG - wrong) << " incorrect guesses left.\n";
			cout << "\nYou've used the following letters:\n" << used << endl;
			cout << "\nSo far, the word is:\n" << soFar << endl;

			char guess;
			cout << "\n\nEnter your guess: ";
			cin >> guess;
			guess = toupper(guess); //make uppercase since secret word in uppercase

			while (used.find(guess) != string::npos)
			{
				cout << "\nYou've already guessed " << guess << endl;
				cout << "Enter your guess: ";
				cin >> guess;
				guess = toupper(guess);
			}

			used += guess;

			if (THE_WORD.find(guess) != string::npos)
			{
				cout << "That's right! " << guess << " is in the word.\n";

				// update soFar to include newly guessed letter
				for (int i = 0; i != THE_WORD.length(); ++i)
				if (THE_WORD[i] == guess)
					soFar[i] = guess;
			}
			else
			{
				cout << "Sorry, " << guess << " isn't in the word.\n";
				++wrong;
			}
		}

		// shut down
		if (wrong == MAX_WRONG)
			cout << "\nYou've been hanged!";
		else
			cout << "\nYou guessed it!";

		cout << "\nThe word was " << THE_WORD << endl;
	}
	else
	{
		int guess;

		cout << "The ghost favourite game !! Guess the number~" << endl;
		cout << "Ok, let's get started." << endl;

			// seed the random number generator
			srand((unsigned int)time(NULL));

			int guess_number = rand() % MAX_GUESS_NUM;  // generate random number

			int i;
			for (i = 0; i<MAX_NUM_TRIES; i++)
			{
				cin.clear();
				cout << "Guess number " << i + 1 << " : ";

				// read user input, if not a valid number, then clear the input stream and
				// 'continue' to the start of the for() loop again.

				if (!(cin >> guess))
				{
					cout << "Please enter numbers only." << endl;
					cin.clear();            // clear 'cin' error flag caused by invalid input
					cin.ignore(1000, '\n');  // ignore up to 1000 characters that may be in the buffer
					// until a NewLine (\n) is reached
					continue;
				}

				// check to see if value entered is in range (valid)
				if (guess < 0 || guess > MAX_GUESS_NUM)
				{
					cout << "You must enter a number from 0 to " << MAX_GUESS_NUM - 1 << endl;
					cin.clear();
					continue;  // i.e. back to start of loop
				}

				if (guess == guess_number)
				{
					cout << "Congratulations! You guessed the right number!" << endl;
					break;  // jump out of the loop

				}
				else
				{
					cout << "Sorry, not the right number!  Try again." << endl;
				}

				if (i == MAX_NUM_TRIES - 1)
				{
					cout << "Your tries are used up! The number I was thinking of was " << guess_number << endl;
				}

			}


		cout << "Goodbye! I hope you enjoyed the game." << endl;
	}
}


//**to check if player can exit to the room they wanted to go. 
void searchCurrentRoomExits(vector<string> i, Map& m, Player& p, GhostStore& g)
{
	typedef vector<string> sentence;
	bool foundexit = false;
	//**put previous room name in a string just incase if player cant get in the room they have to be set back to the previous room.
	string previouscurrentroom = m.getCurrentRoom().getRoomName();
	for (sentence::iterator iter = i.begin(); iter != i.end(); iter++)
	{
		if (m.currentRoomExit((*iter)) == true)
		{
			int num1 = rand() % 2 + 1;
			int num2 = rand() % 3 + 1;
			cout << num2 << endl;
			cout << num1 << endl;
			if (num2 == 1)
			{

				if (num1 == 2)
				{
					bool foundword = false;
					do{
						g.setCurrentGhost("AngryGhost");
						g.getCurrentGhost().printGhost();
						g.setCurrentGhostLife(100);
						cout << "Do you want to fight the ghost or run away?" << endl;
						vector<string> input;
						input = getInput();
						if (found(input, "fight"))
						{
							checkFight(i, m, p, g);
							g.setCurrentGhostLife(100);
							foundword = true;
							break;
						}
						else if (found(input, "run"))
						{
							foundword = true;
							int num = rand() % 2 + 1;
							cout << num << endl;
							if (num == 1)
							{
								cout << "You didnt manage to run away but got caught by the ghost!!" << endl;
								checkFight(i, m, p, g);
								break;
							}
							else if (num == 2)
							{
								cout << "You managed to run away from the ghost!" << endl;

							}
							else
							{
								cout << "You entered an invalid choice. Please try again!" << endl;
							}
						}
					} while (foundword == false);

				}
				else if (num1 == 1)
				{
					bool foundword = false;
					g.setCurrentGhost("PlayfullGhost");
					do{
						g.getCurrentGhost().printGhost();
						cout << "Do you want to play with the ghost or run away?" << endl;
						vector<string> input;
						input = getInput();

						if (found(input, "play"))
						{
							Play();
							foundword = true;
							break;
						}
						else if (found(input, "run"))
						{
							foundword = true;
							int num = rand() % 2 + 1;
							cout << num << endl;
							if (num == 1)
							{
								foundword = true;
								cout << "You didnt manage to run away but got caught by the ghost!!" << endl;
								Play();
								break;
							}
							else if (num == 2)
							{
								cout << "You managed to run away from the ghost!" << endl;

							}
						}
						else
						{
							cout << "You entered an invalid choice. Please try again!" << endl;
						}
					} while (foundword == false);
				}
			}
		
				m.setCurrentRoom((*iter));
				foundexit = true;
				//**Check if the door is locked, and if player have a key in their inventory
				if (m.getCurrentRoom().getlocked() == true)
				{
					cout << "The door is locked!! You need a key to open the door." << endl;
					//**if they have key in inventory then they can unlock the door
					if (p.getInventory().checkInInventory("key") == true)
					{
						cout << "hint: I can see that there is a key in your inventory." << endl;
						parse(m, p, g);
						//if player uses key and unlock the room then they can enter the room.
						if (m.getCurrentRoom().getlocked() == false)
						{
							cout << "The door is unlocked!!" << endl;
							cout << endl;
							m.printCurrentRoom();
							m.getCurrentRoom().printObject();
							break;
						}

					}
					//**if they dont have the key then they cant enter that room and will remain on the previous room
					else
					{
						m.setCurrentRoom(previouscurrentroom);
					}

				}
				//**if player enter storeroom without mingvase in inventory then the ghost wont be affraid of them and they will be killed and game over!
				else if (m.getCurrentRoom().getRoomName() == "storeroom")
				{
					if (p.getInventory().checkInInventory("mingvase") == false)
					{
						m.printCurrentRoom();
						m.getCurrentRoom().printObject();
						cout << "You died!! The ghost is not affraid of you and killed you with a glance." << endl;
						p.setHealth(0);
						break;
					}
					else
					{
						m.printCurrentRoom();
						m.getCurrentRoom().printObject();
					}
				}
				//**else it will find the exit of current room and go to the other room.
				else
				{
					m.printCurrentRoom();
					m.getCurrentRoom().printObject();

				}

			}
		}
		//**if an invalid name in entered show error message.
		if (foundexit == false)
		{
			cout << "Exit not found. Try again!" << endl;
		}

	
}

//**check to see if item can be pick up
void checkPick(vector<string> i, Map& m, Player& p)
{
	typedef vector<string> sentence;
	bool pick = false;

	for (sentence::iterator iter = i.begin(); iter != i.end(); iter++)
	{
		//cout << (*iter) << endl;

		if (m.CheckIfObjectInCurrentRoom((*iter)) == true)
		{

			bool invisible = m.getObjectFromCurrentRoom((*iter))->getObjectInvisible();
			bool pickupable = m.getObjectFromCurrentRoom((*iter))->getObjectpickup();
			//**check if item is visible, some item might be hidden and player needs to do sth before item in visible.
			if (invisible == false)
			{
				//**check item if it could be pick up
				if (pickupable == true)
				{
					p.pickup(*m.getObjectFromCurrentRoom((*iter)));
					m.removeObjectFromCurrentRoom((*iter));
					cout << "Picked up " << (*iter) << endl;
					//after items were picked up, change the description of the room.
					if ((*iter) == "note")
					{
						m.setCurrentRoomDesc("You are now in the living room.Under the dim light, theres a tea table and an old stinky sofa in the middle of the room. The main door is securely locked, you can hear that the strong wind outside is trying hard to break the door.");
					}
					if ((*iter) == "water")
					{
						m.setCurrentRoomDesc("The wall on the kitchen is full of spider webs.Theres a dining table with ten chairs, it might used to be a big family staying in here.");
					}
					if ((*iter) == "key")
					{
						m.setCurrentRoomDesc("Theres a big screen in the movie room.On the table, there is a DVD player which is connected to a switch that might be still working.");
					}
					if ((*iter) == "mingvase")
					{
						m.setCurrentRoomDesc("It is the bedroom of the previous owner.It looks dark and creepy! ");
					}
					if ((*iter) == "masterkey")
					{
						m.setCurrentRoomDesc("Store room is full of groceries cover with very thick of dust and spider web.");
					}
					pick = true;
					break;
					//have to edit room description if item is removed from room.
				}
				else
				{
					cout <<"Sorry "<<p.getName() <<", you cant pick up " <<(*iter)<<"." << endl;
				}
			}
			//**if item is hidden show error message
			else
			{
				cout << "The item is hidden.You need to do something before picking the thing up. hint= 'move','use xx with'" << endl;
			}


			break;
		}
		
	}
	if (pick == false)
	{
		cout << "Sorry " << p.getName() << ", there is no such thing. Please try again!" << endl;
	}
}

//**check item's description
void examineObject(vector<string> i, Map& m, Player& p)
{

	typedef vector<string> sentence;
	bool found = false;

	for (sentence::iterator iter = i.begin(); iter != i.end(); iter++)
	{
		//cout << (*iter) << endl;

		if (m.CheckIfObjectInCurrentRoom((*iter)) == true)
		{
			
			bool invisible = m.getObjectFromCurrentRoom((*iter))->getObjectInvisible();
			bool readable = m.getObjectFromCurrentRoom((*iter))->getObjectReadable();
			//cout << "readable: " << readable << endl;
			if (invisible == false)
			{
				if (readable == true)
				{
					found = true;
					cout << "You have to pick up first." << endl;
					break;
				}
				else
				{
					found = true;
					string desc = m.getObjectFromCurrentRoom((*iter))->getObjectDescription();
					cout << "Object in room: " << (*iter) << endl;
					cout << desc << endl;
					break;
				}
			}
			
			

			//break;
		}
		else if (p.checkInventory((*iter)) == true)
		{

			bool readable = p.getInventory().getObjectByName((*iter))->getObjectReadable();
			if (readable == true)
			{
				found = true;
				cout << "It is a reading material. hint: use 'read'." << endl;
				break;
			}
			else
			{
				found = true;
				string desc = p.getInventory().getObjectByName((*iter))->getObjectDescription();
				cout << "Object in inventory: " << (*iter) << endl;
				cout << desc << endl;
				break;
			}
		}
	}
	if (found == false)
	{
		cout << "Sorry " << p.getName() << ", there is no such thing. Please try again!" << endl;
	}
	//parse(m, p);
}

//**check if item is readable, item must be in inventory.
void readObject(std::vector<std::string> i, Map& m, Player& p)
{
	typedef vector<string> sentence;
	bool found = false;

	for (sentence::iterator iter = i.begin(); iter != i.end(); iter++)
	{
		if (p.checkInventory((*iter)) == true)
		{
			found = true;
			bool readable = p.getInventory().getObjectByName((*iter))->getObjectReadable();
			string desc = p.getInventory().getObjectByName((*iter))->getObjectDescription();
			if (readable == true)
			{
				cout << endl;
				cout << desc << endl;
				cout << endl;
				break;
			}
		}
		if (m.CheckIfObjectInCurrentRoom((*iter)) == true)
		{

			bool invisible = m.getObjectFromCurrentRoom((*iter))->getObjectInvisible();
			bool readable = m.getObjectFromCurrentRoom((*iter))->getObjectReadable();
			//cout << "readable: " << readable << endl;
			if (invisible == false)
			{
				if (readable == true)
				{
					found = true;
					cout << "You have to pick up first." << endl;
					break;
				}
			}
		}
		
	}
	if (found == false)
		{
			cout << "Sorry " << p.getName() << ", item not readable." << endl;

		}
}

//**check and move onject
void moveObject(std::vector<std::string> i, Map& m,Player& p)
{
	typedef vector<string> sentence;
	bool moved = false;

	for (sentence::iterator iter = i.begin(); iter != i.end(); iter++)
	{
		if (m.CheckIfObjectInCurrentRoom((*iter)) == true)
		{
			string disk = "disk";
			bool movable = m.getObjectFromCurrentRoom((*iter))->getObjectMoveable();
			//**move sofa and make disk visible for pick up
			if (movable == true)
			{
				m.getObjectFromCurrentRoom((*iter))->setObjectMoveable(false);
				
				string desc=m.getObjectFromCurrentRoom((*iter))->getmove();
				cout << desc << endl;
				m.getObjectFromCurrentRoom(disk)->setObjectInvisible(false);
			}
			else
			{
				cout << (*iter)<< " is not movable" << endl;
			}

			//parse(m, p);

			break;
		}

	}

}

void drinkObject(std::vector<std::string> i, Map& m, Player& p)
{
	typedef vector<string> sentence;
	bool drinked = false;


	for (sentence::iterator iter = i.begin(); iter != i.end(); iter++)
	{
		//cout << (*iter) << endl;

		if (p.checkInventory((*iter)) == true)
		{
			cout << "I found water in inventory" << endl;
			bool drinkable = p.getInventory().getObjectByName((*iter))->getObjectDrinkable();
			if (drinkable == true)
			{	
				//cout << "Found drinkable" << endl;
				drinked = true;
				p.removeFromInventory((*iter));
				cout << "You just drinked the glass of water.The water there was long enough on the table. Hope you wont feel sick after that." << endl;
			}
		}
		
	//break;
	}
		
	if (drinked = false)
	{
		cout <<"It's not drinkable, are you out of your mind?!" << endl;
	}

}

//**check if Item can be used with sth
void useObject(std::vector<std::string> i, Map& m, Player& p)
{
	typedef vector<string> sentence;
	bool usewith = false;
	string with;
	string disk = "disk";
	string key = "key";
	string mingvase = "mingvase";
	string masterkey = "masterkey";

	string usewithobject;
	for (sentence::iterator iter = i.begin(); iter != i.end(); iter++)
	{
		//cout << (*iter) << endl;

		if (p.checkInventory((*iter)) == true)
		{
			cout << "with?" << endl;
			cin >> with;
			usewithobject=p.getInventory().getObjectByName((*iter))->getUseWith();
			
			if (m.CheckIfObjectInCurrentRoom((with)) == true)
			{
				//cout << with << "is in the room" << endl;
				//**if object1's use with is object2, then object2's use with must match with object1's name. 
				if (with == usewithobject)
				{
					//cout << "use with is the same as object" << endl;
					p.removeFromInventory((*iter));

					if ((*iter) == disk)
					{
						cout << endl;
						cout << "Something is projected to the wall, its not very clear but good enough to listen what he was trying to say:" << endl;
						cout << endl;
						cout << "Hi, I knew I wont survive long,so I decided to tape this as if someone is trapped in this house they will need this to get out of the house.The ghost is a ghost from ancient times, the ghost lock people in this house and the master key of the house is with him. Anyway,it is affraid of the ming vase so by any chance if you come accross a ming vase you have to use it on the ghost, once you kill the ghost you will be able to get out of this house."<<endl;
						cout << "What i could say is Goodluck." << endl;
						cout << endl;
						cout << "The screen went black and the room is dark." << endl;
						break;
					}
					//**if key was used with door then make mingvase visible for pick up
					if ((*iter) == key)
					{
						string bedroom = "bedroom";
						string mingvase = "mingvase";
						m.getRoom(bedroom)->setlocked(0);
						m.setCurrentRoom(bedroom);
						m.getObjectFromCurrentRoom(mingvase)->setObjectInvisible(0);
						//cout << "Room is unlocked 2"<< endl;
						cout << endl;

						break;
					}
					if ((*iter) == mingvase)
					{
						cout << "The ghost was suck into the vase and dissappear leaving the shiny masterkey on the floor!" << endl;
						cout << "What are you waiting for?!!" << endl;
						string masterkey = "masterkey";
						string ghost = "ghost";
						m.getObjectFromCurrentRoom(masterkey)->setObjectInvisible(0);
						m.removeObjectFromCurrentRoom(ghost);
					}
					if ((*iter) == masterkey)
					{
						p.setHealth(105);

					}
					//cout << "Romoved from inventory" << endl;
				}
			}
		}
		

	}
	//break;
}




void parse(Map& currentMap, Player& currentPlayer, GhostStore& ghost)
{
	vector<string> input;
		
		input=getInput();

	if (found(input, "go"))
	{
		searchCurrentRoomExits(input, currentMap, currentPlayer , ghost);
	}
	else if (found(input, "pick"))
	{
		checkPick(input, currentMap,currentPlayer);
	}
	else if (found(input, "examine"))
	{
		examineObject(input, currentMap, currentPlayer);
	}
	else if (found(input, "read"))
	{
		readObject(input, currentMap, currentPlayer);
	}
	else if (found(input, "move"))
	{
		moveObject(input, currentMap,currentPlayer);
	}
	else if (found(input, "drink"))
	{
		drinkObject(input, currentMap, currentPlayer);
	}
	else if (found(input, "inventory"))
	{
		currentPlayer.getInventory().printInventory();
		
	}
	else if (found(input, "health"))
	{
		int health = currentPlayer.getHealth();
		cout << "Current health: " << health << endl;;
	}
	else if (found(input, "use"))
	{
		useObject(input, currentMap, currentPlayer);
		cin.ignore(1);
	}
	else if (found(input, "exit"))
	{
		cout << "Are you sure you want to quit?(y/n)" << endl;
		string quit;
		cin >> quit;
		if (quit == "y")
		{
			cout << "Do you want to save current game?(y/n)" << endl;
			string save;
			cin >> save;
			if (save == "y")
			{
				currentMap.saveRooms();
				currentMap.saveObjects();
				currentPlayer.savePlayer();
				currentPlayer.getInventory().saveInventory(); 

			}
			else if (save == "n")
			{
				currentPlayer.setHealth(0);
			}
		}

	}
	else if (found(input, "main"))
	{
		system("CLS");
		Intro();
		startGame();
	}
	else if (found(input, "fight"))
	{
		//if shoud open new function for fight,hide,run
		//if should create ghost.h
		//if there is 25% a ghost on the way they can choose to fight.
		checkFight(input, currentMap, currentPlayer,ghost);

	}
	else if (found(input, "rest"))
	{
		currentPlayer.setHealth(100);
	}
	else if (found(input, "save"))
	{
		string roomName = currentMap.getCurrentRoom().getRoomName();
		currentMap.setCurrentRoom(roomName);
		cout << roomName << endl;
		currentMap.saveRooms();
		currentMap.saveObjects();
		currentMap.saveCurrentRoom(roomName);
		currentPlayer.savePlayer();
		currentPlayer.getInventory().saveInventory();
		cout << "Game saved at current stage." << endl;

	}
	/*else
	{
		cout << "Sorry " << currentPlayer.getName() << ", you entered an invalid comand.Please try again!" << endl;
	}*/

}

bool getSaved()
{
	ifstream inFile("SavePlayer.txt");

	//if the file opened successfully save everything
	if (inFile)
	{
		return true;
	}
	else
	{
		return false;
	}
}


