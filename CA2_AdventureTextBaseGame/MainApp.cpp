#include "Rooms.h"
#include "Player.h"
#include "Objects.h"
#include "Inventory.h"
#include"Input.h"
#include"ParseInput.h"
#include "GameFunctions.h"
#include"StringFunction.h"

#include<string>
#include<vector>
#include<iostream>
//**for input and output file
#include<fstream>

using std::string;		using std::vector;
using std::cout;		using std::cin;
using std::endl;		using std::ifstream;
using std::ostream;		//using namespace std;

int main()
{
	//**run thru the intro
	Intro();
	//**starting the game
	startGame();


	//the command prompt wont close straight away after player finishes game
	cout << "Level Completed! Continue to another level? (y/n)" << endl;
	string input;
	cin >> input;
	if (input == "y")
	{
		startGame();
	}
	else if (input == "n")
	{
		cout << "Goodbye!" << endl;
	}
	system("PAUSE");

	return 0;
}

