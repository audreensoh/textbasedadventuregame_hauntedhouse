#ifndef GAME_FUNCTION
#define GAME_FUNCTION

//**Contains function for starting the game and calling the funtions from classes

#include "Map.h"
#include "Player.h"
#include "GhostStore.h"

void Intro();
void mainMenu();
void mainMenuChoice(int x);
void loadMap(Map& Map1);
void loadPlayer(Player& p);
void loadGhost(GhostStore& gs);
void savePlayer(Player& p);
void saveMap(Map& m);
void startGame();


#endif