#include<iostream>
#include<string>
#include<vector>
#include <fstream>
#include "Rooms.h"
#include "StringFunction.h"

using std::string;
using std::vector;
using std::endl;
using std::cout;
using std::istream;
using std::ofstream;
//just to incase
//using namespace std;

Room::Room()
{
	roomNumber = 0;
	roomName = "No name";
	roomDesc = "No desc";
	roomExit = 0;
	allExits.push_back("No exit found");
}

Room::Room(int number, string name, string desc, bool locked, int exitno, const vector<string>& exitPlace)
{
	roomNumber = number;
	roomName = name;
	roomDesc = desc;
	roomLocked = locked;
	roomExit = exitno;
	allExits = exitPlace;
}

void Room::setRoomNumber(int n)
{
	roomNumber = n;
}
int Room::getRoomNumber()
{
	return roomNumber;
}

void Room::setRoomName(string n)
{
	roomName = n;
}

string Room::getRoomName()
{
	return roomName;
}

void Room::setRoomDesc(string d)
{

	roomDesc = d;
	//cout << "Changed to:     "<<roomDesc << endl;
}

string Room::getRoomDesc()
{
	return roomDesc;
}

void Room::setlocked(bool n)
{
	roomLocked = n;
}

bool Room::getlocked()
{
	//cout << "room locked: " << roomLocked << endl;
	return roomLocked;
}

int Room::getexitno()
{
	return roomExit;
}

void Room::setExitNo(int n)
{
	roomExit = n;
}

void Room::setExits(vector<string> exit)
{
	allExits = exit;
}

vector<string> Room::getExits()
{
	return allExits;
}

bool Room::findExit(string e)
{
	for (vector<string>::size_type i = 0; i < allExits.size(); i++)
	{
		//cout << allExits.size() << endl;
		//cout << "looking for exit   " << allExits[i] << endl;
		if (allExits[i] == e)
		{
			//cout << "Found Exit" << endl;
			return true;
			break;
		}
		else
		{
			//cout << "didnt Found Exit" << endl;
		}
	}
	
	return false;
}

void Room::addObject(Object& o)
{
	allobject.push_back(o);
}

bool Room::removeObject(string& o)
{
	bool removed = false;
	for (vector<Object>::size_type i = 0; i != allobject.size(); i++)
	{
		if (allobject[i].getObjectName() == o)
		{

			allobject.erase(allobject.begin() + i);
			cout << o << " is removed." << endl;
			removed = true;
			break;
		}
	}
	if (removed == false)
	{
		cout << "Object not found in inventory." << endl;
	}
	return removed;
}

bool Room::checkObjectInRoom(string& n)
{
	bool check = false;
	for (vector<Object>::size_type i = 0; i != allobject.size(); i++)
	{
		if (allobject[i].getObjectName() == n)
		{
			n = allobject[i].getObjectName();
			check = true;
			break;
		}
	}
	return check;
}

Object* Room::getObject(string& n)
{
	bool check = false;
	for (vector<Object>::size_type i = 0; i != allobject.size(); i++)
	{
		if (allobject[i].getObjectName() == n)
		{
			n = allobject[i].getObjectName();
			check = true;
			return &allobject[i];
			break;
		}
	}
	return NULL;
}



void Room::printRoom()
{
	cout << endl;
	center("~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~");
	center("");
	center(roomName);
	center("");
	center("~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~");
	cout << "Description:" << endl;
	cout<< roomDesc << endl;
	cout << "There are " << roomExit << " exit(s)." << "You can exit to ";
	for (vector<string>::size_type i = 0; i < allExits.size(); i++)
	{
		cout << " -" << allExits[i];
	}


	cout << endl;
}

void Room::printObject()
{
	//cout << "printing objects in room:" << endl;
	for (vector<Object>::size_type i = 0; i != allobject.size(); i++)//print objects
	{
		
		//cout << "-- " << allobject[i].getObjectName() << endl;
		
	}
}

void Room::saveObjects(ofstream& myfile)
{
	
	
	for (int i = 0; i != allobject.size(); i++)
	{
		
		
		myfile << "Object= " << allobject[i].getObjectName() << endl;
		myfile << "description= " << allobject[i].getObjectDescription() << endl;
		myfile << "Where= " << allobject[i].getObjectWhere() << endl;
		myfile << "pickup= " << allobject[i].getObjectpickup() << endl;
		myfile << "movable= " << allobject[i].getObjectMoveable() << endl;
		myfile << "move= " << allobject[i].getmove() << endl;
		myfile << "useable= " << allobject[i].getObjectUsable() << endl;
		myfile << "invisible= " << allobject[i].getObjectInvisible() << endl;
		myfile << "useWith= " << allobject[i] .getUseWith()<< endl;
		myfile << "readable= " << allobject[i].getObjectReadable() << endl;
		myfile << "drinkable= " << allobject[i].getObjectDrinkable() << endl;

	}
}

vector<Object> Room::getAllObject()
{
	return allobject;
}