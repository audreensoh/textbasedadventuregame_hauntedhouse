#ifndef PARSEINPUT_H
#define PARSEINPUT_H

//**Parse all the inputs from the player and check if there is the command word

#include<vector>
#include<string>
#include"Rooms.h"
#include"Map.h"
#include"Inventory.h"
#include"Player.h"
#include"Input.h"
#include"Objects.h"
#include"GhostStore.h"


void parse(Map& currentMap, Player& currentPlayer,GhostStore& ghost);
void found();
int meetGhost();
void Play();

void searchCurrentRoomExits(std::vector<std::string> i, Map& m, Player& p, GhostStore& g);
void checkPick(std::vector<std::string> i, Map& m, Player& p);
void examineObject(std::vector<std::string> i, Map& m, Player& p);
void readObject(std::vector<std::string> i, Map& m, Player& p);
void moveObject(std::vector<std::string> i, Map& m, Player& p);
void drinkObject(std::vector<std::string> i, Map& m, Player& p);
void useObject(std::vector<std::string> i, Map& m, Player& p);
void checkfight(std::vector<std::string> i, Map& m, Player& p);


bool getSaved();
#endif