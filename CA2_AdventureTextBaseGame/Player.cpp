#include"Player.h"
#include<iostream>
#include<fstream>

using std::endl;
using std::cout;
using std::string;
using std::ofstream;
using std::vector;
//using namespace std;

Player::Player()
{

}

Player::Player(string name, int health, Inventory i)
{
	pname = name;
	phealth = health;
	pInventory = i;
}

Player::~Player()
{

}

void Player::setName(string n)
{
	pname = n;
}

string Player::getName()
{
	return pname;
}

void Player::setHealth(int h)
{
	phealth = h;
}

int Player::getHealth()
{
	return phealth;
}

void Player::setExperience(int e)
{
	pExperience = e;
}

int Player::getExperience()
{
	return pExperience;
}

Inventory Player::getInventory()
{
	return pInventory;
}

void Player::removeFromInventory(Object& o)
{
	pInventory.removeFromInventory(o);
}

void Player::removeFromInventory(string n)
{
	pInventory.removeFromInventory(n);
}

void Player::pickup(Object& o)
{
	pInventory.addToInventory(o);
}

bool Player::checkInventory(string& n)
{
	return pInventory.checkInInventory(n);
}

void Player::savePlayer()
{
	ofstream myfile;
	myfile.open("SavePlayer.txt");

	{
		myfile << "PlayerName= " << pname << endl;
		myfile << "Health= " << phealth << endl;
		myfile << "Experience= " << pExperience << endl;

	}
}