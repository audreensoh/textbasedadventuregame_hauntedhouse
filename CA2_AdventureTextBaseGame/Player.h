#ifndef PLAYER_H
#define PLAYER_H

//**contains player info and inventory
#include"Inventory.h"
#include<string>
#include<fstream>


class Player
{
private:
	std::string pname;
	int phealth;
	//**to store player's inventory in player
	Inventory pInventory;
	int pExperience;

public:
	Player();
	Player(std::string name, int health, Inventory i);
	~Player();
	void setName(std::string n);
	std::string getName();
	void setHealth(int h);
	int getHealth();
	void setExperience(int e);
	int getExperience();
	
	bool metAngryGhost();
	bool metPlayGhost();
	void setMetAngryGhost(bool b);
	void setMetPlayGhost(bool b);

	Inventory getInventory();
	void removeFromInventory(Object& o);
	void removeFromInventory(std::string n);
	void pickup(Object& o);
	bool checkInventory(std::string& n);

	void savePlayer();

};

#endif