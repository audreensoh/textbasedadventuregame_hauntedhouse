#include "Objects.h"
#include <iostream>

using std::string;
using std::cout;
using std::cin;
using std::endl;
//using namespace std;

Object::Object()
{

}

Object::Object(string name, string description,string where, string useWith, bool pickup, bool movable,string move,bool drink, bool useable, bool invisible, bool readable)
{
	oname = name;
	odescription = description;
	owhere = where;
	ouseWith = useWith;
	opickup = pickup;
	omovable = movable;
	omove = move;
	odrinkable = drink;
	ouseable = useable;
	oinvisible = invisible;
	oreadable = readable;
	
}

Object::~Object()
{

}

string Object::examin()
{
	return odescription;
}

string Object::getObjectName()
{
	return oname;
}

void Object::setObjectName(string name)
{
	oname = name;
}

string Object::getObjectDescription()
{
	return odescription;
}

void Object::setObjectDescription(string Desc)
{
	odescription = Desc;
}

string Object::getObjectWhere()
{
	return owhere;
}

void Object::setObjectWhere(string where)
{
	owhere = where;
}

void Object::setObjectInvisible(bool visible)
{
	oinvisible = visible;
}

bool Object::getObjectInvisible()
{
	return oinvisible;
}
void Object::setObjectpickup(bool pickup)
{
	opickup = pickup;
}

bool Object::getObjectpickup()
{
	return opickup;
}

void Object::setObjectMoveable(bool move)
{
	omovable = move;
}

bool Object::getObjectMoveable()
{
	return omovable;
}

string Object::getmove()
{
	//cout << omove << endl;
	return omove;
}


void Object::setObjectDrinkable(bool drink)
{
	odrinkable = drink;
}

bool Object::getObjectDrinkable()
{
	return odrinkable;
}

void Object::setusewith(string usewith)
{
	ouseWith = usewith;
}

string Object::getUseWith()
{
	return ouseWith;
}

void Object::setObjectUsable(bool use)
{
	ouseable = use;
}

bool Object::getObjectUsable()
{
	return ouseable;
}

void Object::setObjectReadable(bool read)
{
	oreadable = read;
}

bool Object::getObjectReadable()
{
	return oreadable;
}

void Object::setmovedDesc(string moved)
{
	omove = moved;
}


void Object::printObject()
{
	cout << "Object = " << oname << endl;
	cout << "Description = " << odescription << endl;
	cout << "Movable = " << omovable;
	cout << "use with = " << ouseWith << endl;
	cout << "moved = " << omove;

}